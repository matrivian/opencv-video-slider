#include "slider.hpp"

void onTrackbarSlide(int pos, void *param)
{
    Slider *slider = (Slider*) param;

    slider->cap.set(cv::CAP_PROP_POS_FRAMES, pos);
    if (!slider->dontset)
        slider->run = 1;
    slider->dontset = 0;
}

void Slider::slide(char *video_name)
{
    Slider video_slider;
    std::string win_name = "Video";
    cv::namedWindow(win_name, 1);
    video_slider.cap.open(video_name);
    int frames = (int) video_slider.cap.get(cv::CAP_PROP_FRAME_COUNT);
    int tmpw   = (int) video_slider.cap.get(cv::CAP_PROP_FRAME_WIDTH);
    int tmph   = (int) video_slider.cap.get(cv::CAP_PROP_FRAME_HEIGHT);
    std::cout << "Video has " << frames << " frames of dimensions(" 
              << tmpw << ", " << tmph << ")." << std::endl;
    cv::createTrackbar("Position", win_name, &video_slider.slider_position, frames, 
      onTrackbarSlide,                      // callback function  
      (Slider*)&video_slider);
    cv::Mat frame;
    while(1)
    {
        if (video_slider.run != 0)
        {
            video_slider.cap >> frame; 
            if (!frame.data) break;
            int current_pos = (int) video_slider.cap.get(cv::CAP_PROP_POS_FRAMES);
            video_slider.dontset = 1;
            cv::setTrackbarPos("Position", win_name, current_pos);
            cv::imshow(win_name, frame);
            video_slider.run -= 1;
        }
        char c = (char) cv::waitKey(10);
        if (c == 's') // single step
        {
            video_slider.run = 1;
            std::cout << "Single step, run = " << video_slider.run << std::endl;
        }
        if (c == 'r') // run mode
        {
            video_slider.run -= 1;
            std::cout << "Run mode, run = " << video_slider.run << std::endl;
        }
        if (c == 27)
            break;
    }
}
