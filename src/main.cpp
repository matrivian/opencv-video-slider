#include "slider.hpp"

int main(int argc, char **argv)
{
    Slider vid_slider;
    if (argc == 2)
        vid_slider.slide(argv[1]);
    else
        std::cout << "Please specify video name" << std::endl;

    return 0;
}
