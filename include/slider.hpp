#ifndef VIDEO_SLIDER_H_
#define VIDEO_SLIDER_H_

#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>
#include <fstream>

class Slider{
public:
    int slider_position; 
    int run;
    int dontset;
    cv::VideoCapture cap;
    Slider(){
        slider_position = 0;
        run             = 1;
        dontset         = 0;
    }
    void slide(char *video_name);
};

void onTrackbarSlide(int pos, void *param);

#endif // VIDEO_SLIDER_H_
